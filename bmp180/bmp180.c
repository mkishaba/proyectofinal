#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include "bmp180.h"

typedef struct bmp180_coeff_s
{
  short int ac1;
  short int ac2;
  short int ac3;
  unsigned short int ac4;
  unsigned short int ac5;
  unsigned short int ac6;
  short int b1;
  short int b2;
  short int mb;
  short int mc;
  short int md;
} bmp180_coeff_t;

const unsigned int conversion_delay[4]={6, 10, 15, 30};

int sensorInit(void) {
  int fd;
  fd = open("/dev/td3_i2c", O_RDWR);
  if (fd < 0) {
    printf("no se pudo abrir td3_i2c\n");
    return -1;
  } else {
    return fd;
  }
}

int delay_ms(unsigned int msec) { 
  int ret;
  struct timespec a;

  if (msec>999) { 
    fprintf(stderr, "delay_ms error: delay value needs to be less than 999\n");
    msec=999;
  }

  a.tv_nsec=((long)(msec))*1E6d;
  a.tv_sec=0;

  if ((ret = nanosleep(&a, NULL)) != 0) { 
    fprintf(stderr, "delay_ms error: %s\n", strerror(errno));
  }
  return(0);
}


void leeTemperaturaYPresion(int fd, float *temperatura, float *presion) {
  int i;
  unsigned char tval;
  bmp180_coeff_t coeff;
  unsigned char *cbuf;
  unsigned char buf[10];
  unsigned char oss;
  long ut;
  long up;
  long x1, x2, x3;
  long b3, b4, b5, b6, b7;
  long p;
  long t;
  float deg;
  float kpa;
  float alt;

  oss=3; // pressure conversion mode: 3=ultra-high-res, down to 0 which is ultra-low-power
  cbuf=(unsigned char *)&coeff;

  // read BMP085 coefficients (22 bytes)
  buf[0] = 0xaa;
  write(fd, buf, 1);
  read(fd, cbuf, 22);

  // swap the order of bytes due to endianness
  for (i=0; i<22; i=i+2)
  {
    tval=cbuf[i];
    cbuf[i]=cbuf[i+1];
    cbuf[i+1]=tval;
  }

  // read uncompensated temperature register
  buf[0]=0xf4;
  buf[1]=0x2e;
  write(fd, buf, 2);
  delay_ms(6);
  buf[0] = 0xf6;
  write(fd, buf, 1);
  read(fd, buf, 2);
  ut=(((long)buf[0])<<8) | (long)buf[1];
  
  buf[0]=0xf4;
  buf[1]=0x34+(oss<<6);
  write(fd, buf, 2);
  delay_ms(conversion_delay[oss]);
  buf[0] = 0xf6;
  write(fd, buf, 1);
  read(fd, buf, 3);
  up=(((long)buf[0])<<16) | (((long)buf[1])<<8) | (long)buf[2];
  up=up>>(8-oss);

  // now do the calculations for temperature
  x1=(ut-(long)coeff.ac6)*((long)coeff.ac5)/32768;
  x2=((long)coeff.mc)*2048/(x1+(long)coeff.md);
  b5=x1+x2;
  t=(b5+8)/16;
  deg=((float)t)/10;

  // calculations for the pressure
  b6=b5-4000;
  x1=(((long)coeff.b2)*(b6*b6/4096))/2048;
  x2=((long)coeff.ac2)*b6/2048;
  x3=x1+x2;
  
  b3=(((((long)coeff.ac1)*4+x3)<<oss) +2)/4;
  x1=(((long)coeff.ac3)*b6)>>13;
  x2=(((long)coeff.b1)*(b6*b6>>12))>>16;
  x3=(x1+x2+2)>>2;
  b4=((long)coeff.ac4)*(unsigned long)(x3+32768)/32768;
  b7=((unsigned long)up - b3)*(50000>>oss);
  if (b7<0x80000000)
    p=(((unsigned long)b7)<<1)/b4;
  else
    p=(((unsigned long)b7)/b4)*2;

  x1=(p/256);
  x1=x1*x1;  
  
  x1=(x1*3038)>>16;
  x2=(-7357*p)>>16;
  p+=(x1+x2+3791)>>4;
  kpa=((float)p)/100;

  *temperatura = deg;
  *presion = kpa;
}

/* Para testear el sensor independientemente
int main(int argc, char *argv[]) { 
  float temperatura = 0;
  float presion = 0;
  int fd;

  fd = sensorInit();
  if (fd > 0) {
    leeTemperaturaYPresion(fd, &temperatura, &presion);
    printf("Temp.   %.1f C\n", temperatura);
    printf("Presion %.3f hPa\n", presion);
  } else {
    printf("Error al inicializar el sensor!\n");
    return -1;
  }
  close(fd);
  return(0);
}
*/
