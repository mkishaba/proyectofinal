#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "./tcpSockets.h"

void *get_in_addr(struct sockaddr *sa) {
  if (sa->sa_family == AF_INET) {
    return &(((struct sockaddr_in*)sa)->sin_addr);
  }
  return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

// Esta función retorna una referencia a un socket bindeado en el puerto pasado
// por parámetro en caso de éxito y -1 en caso de error
int creaSocketTcpYBindea(char *puerto) {
  struct addrinfo hints;
  struct addrinfo *listaAddrInfo;
  struct addrinfo *ptr;
  int sockfd;
  int res;
  int yes = 1;

  // Cargo información necesaria para crear el socket por donde va a recibir
  // conexiones el servidor
  memset((void *) &hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;     // No me interesa si es IPv4 o IPv6  
  hints.ai_socktype = SOCK_STREAM; // Para crear un socket TCP
  hints.ai_flags = AI_PASSIVE;     // Para que use mi IP

  // Llamo a la función "getaddrinfo" para obtener una lista de estructuras
  // del tipo addrinfo con la información cargada y así poder crear, a 
  // partir de éstas, un socket TCP
  res = getaddrinfo(NULL, puerto, &hints, &listaAddrInfo);
  if (res != 0) {
    printf("Error en getaddrinfo!\n");
    return -1;
  }

  // Creo un socket y realizo el bind con la primer estructura con datos válidos
  // de la lista que devolvió "getaddrinfo".
  for (ptr = listaAddrInfo; ptr != NULL; ptr = ptr->ai_next) {
    if ((sockfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol)) == -1) {
      // No se pudo crear el socket con este elemento de la lista, sigo iterando
      continue;
    }
    if ((setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int))) == -1) {
      // No se pudo configurar el socket
      return -1;
    }
    if ((bind(sockfd, ptr->ai_addr, ptr->ai_addrlen)) == -1) {
      // No se pudo bindear el socket
      continue;
    }
    break; // Si terminaron bien todas las condiciones significa que se creó y bindeo
           // correctamente el socket -> salgo del ciclo for
  }

  freeaddrinfo(listaAddrInfo); // Libero la memoria de la lista de estructuras addrinfo

  if (ptr == NULL) {
    // Si entró acá significa que no se pudo crear o bindear el socket correctamente
    printf("No se pudo crear o bindear correctamente el socket!\n");
    return -1;
  }
  return sockfd;
}

int creaSocketTcpYConecta(char *serverIP, char *puertoServer) {
  struct addrinfo hints;
  struct addrinfo *listaAddrInfo;
  struct addrinfo *ptr;
  int sockfd;
  int res;

  memset((void *) &hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;     // No me interesa si es IPv4 o IPv6  
  hints.ai_socktype = SOCK_STREAM; // Para crear un socket TCP

  // Llamo a la función "getaddrinfo" para obtener una lista de estructuras
  // del tipo addrinfo con la información cargada y así poder crear, a 
  // partir de éstas, un socket TCP
  res = getaddrinfo(serverIP, puertoServer, &hints, &listaAddrInfo);
  if (res != 0) {
    printf("Error en getaddrinfo!\n");
    return -1;
  }

  for (ptr = listaAddrInfo; ptr != NULL; ptr = ptr->ai_next) {
    if ((sockfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol)) == -1) {
      // No se pudo crear el socket con este elemento de la lista, sigo iterando
      continue;
    }
    if ((connect(sockfd, ptr->ai_addr, ptr->ai_addrlen)) == -1) {
      // No se pudo hacer el connect del socket
      close(sockfd);
      continue;
    }
    break; // Si terminaron bien todas las condiciones significa que se creó y se conectó
           // correctamente el socket -> salgo del ciclo for
  }

  freeaddrinfo(listaAddrInfo); // Libero la memoria de la lista de estructuras addrinfo

  if (ptr == NULL) {
    // Si entró acá significa que no se pudo crear o bindear el socket correctamente
    printf("No se pudo crear o conectar correctamente el socket!\n");
    return -1;
  }
  return sockfd;
}
