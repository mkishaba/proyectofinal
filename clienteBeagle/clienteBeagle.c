#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include "../tcpSockets/tcpSockets.h"
#include "../bmp180/bmp180.h"

int main(int argc, char *argv[]) {
  int socketClienteServidor;
  int total;
  int enviados;
  int recibidos;
  int bytes;
  int fd;
  float temperatura=0;
  float presion=0;
  char respuesta[4096];
  char hostName[] = "pomunion.esy.es";
  char puertoHttp[] = "80";
  char mensaje[4096];
  char datos[64];
  char comando[]="GET /pf/Services/sendData.php?user=u920835913_pf&pass=SR7bvtMARviv&user_key=1&fecha=2019-07-19%2000:00:00";
  char httpHeader[]=" HTTP/1.0\r\nHost: pomunion.esy.es\r\nContent-Type: text/plain\r\n\r\n";

  socketClienteServidor = creaSocketTcpYConecta(hostName, puertoHttp);
  if (socketClienteServidor == -1) {
    perror("Error: socketCliente\n");
    return -1;
  } else {
    printf("Socket OK!\n\n");
  }

  fd = sensorInit();
  if (fd > 0) {
    leeTemperaturaYPresion(fd, &temperatura, &presion);
    printf("Temp.   %.1f C\n", temperatura);
    printf("Presion %.3f hPa\n\n", presion);
  } else {
    printf("Error al inicializar el sensor!\n");
    return -1;
  }

  memset(datos, 0, sizeof(datos));
  memset(mensaje, 0, sizeof(mensaje));
  sprintf(datos, "&data={\"i\":%.2f,\"v\":%.2f,\"t\":%.2f}", temperatura, presion, temperatura);
  sprintf(mensaje, "%s%s%s", comando, datos, httpHeader);
  printf("Request a realizar:\n%s\n", mensaje);

  total = strlen(mensaje);
  enviados = 0;
  do {
    int bytes = write(socketClienteServidor, mensaje+enviados, total-enviados);
    if (bytes < 0)
      printf("ERROR writing message to socket\n");
    if (bytes == 0)
      break;
    enviados+=bytes;
  } while (enviados < total);

  memset(respuesta, 0, sizeof(respuesta));
  total = sizeof(respuesta)-1;
  recibidos = 0;
  do {
    bytes = read(socketClienteServidor, respuesta+recibidos, total-recibidos);
    if (bytes < 0)
      printf("ERROR reading response from socket\n");
    if (bytes == 0)
      break;
    recibidos+=bytes;
  } while (recibidos < total);

  printf("Respuesta del request:\n%s\n", respuesta);

  close(socketClienteServidor);

  return 0;
}
